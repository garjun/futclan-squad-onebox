# name: Fuclan squad Onebox
# about: Onebox for Futclan squad
# version: 0.1
# authors: garj

register_css <<CSS

.squad_image {
  max-width: 100% !important;
}

aside.onebox .futclan_source {
  background: url(http://forum.futclan.com/uploads/default/original/1X/c14d3c5566a6e335fd6a39e06915800a2edc0d29.png) no-repeat;
  padding-left: 20px;
}

CSS

# Without this, all the Onebox stuff isn't loaded,
# so methods like `matches_regexp` do not work.
# It's weird. I know.
Onebox = Onebox
class Onebox::Engine::FutclanSquadOnebox
  include Onebox::Engine
  include HTML

  ## this is needed else generic onebox will kick in
  def self.priority
    0
  end

  ## https://www.futclan.com/squads/1
  ## http://www.futclan.com/squads/1

  REGEX = /^http?:\/\/www.futclan.com\/squads\/([^\/]+)\/?$/

  matches_regexp REGEX

  def to_html
    response = Onebox::Helpers.fetch_response(@url)
    html_doc = Nokogiri::HTML(response.body)
    raw = parse_open_graph(html_doc, @url)

    image = raw[:metadata][:image].first
    title = raw[:metadata][:description].first

    "
    <aside class='onebox whitelistedgeneric'>
      <header class='futclan_source'>
        <a href='#{@url}'>
          www.futclan.com
        </a>
      </header>
      <article class='onebox-body'>
        <a href='#{@url}/image'>
          <img src='#{image}' class='squad_image'>
        </a>
        <h4>
          <a href='#{@url}'>
            #{title}          
          </a>
        </h4>
      </article>
      <div style='clear: both'></div>
    </aside>
    "

  end

  def parse_open_graph(html, og_url)
    og = Struct.new(:url, :type, :title, :description, :images, :metadata, :html).new
    og.url = og_url
    og.images = []
    og.metadata = {}

    attrs_list = %w(title url type description)
    html.css('meta').each do |m|
      if m.attribute('property') && m.attribute('property').to_s.match(/^og:/i)
        m_content = m.attribute('content').to_s.strip
        m_name = m.attribute('property').to_s.gsub('og:', '')
        og.metadata[m_name.to_sym] ||= []
        og.metadata[m_name.to_sym].push m_content
        if m_name == "image"
          image_uri = URI.parse(m_content) rescue nil
          if image_uri
            if image_uri.host.nil?
              image_uri.host = URI.parse(og_url).host
            end
            og.images.push image_uri.to_s
          end
        elsif attrs_list.include? m_name
          og.send("#{m_name}=", m_content) unless m_content.empty?
        end
      end
    end

    og
  end

end
